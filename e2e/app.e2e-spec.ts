import { ModulrPage } from './app.po';

describe('modulr App', function() {
  let page: ModulrPage;

  beforeEach(() => {
    page = new ModulrPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
